FROM cm2network/steamcmd:latest

ENV V_RISING_DAY_DURATION_SECONDS=1080.0
ENV V_RISING_DAY_START_HOUR=9
ENV V_RISING_DAY_END_HOUR=17

ENV V_RISING_PORT=9876
ENV V_RISING_QUERY_PORT=9877
ENV V_RISING_MAX_USER=40
ENV V_RISING_MAX_ADMIN=4
ENV V_RISING_DESC=""
ENV V_RISING_CLAN_SIZE=4

ENV V_RISING_SETTING_PRESET=""
ENV V_RISING_GAME_MODE="PvP"

ENV V_RISING_PUBLIC_LIST=true
ENV V_RISING_SAVE_NAME="world1"
ENV V_RISING_PASSW=""

USER root

RUN apt update -y && apt install -y wine gettext-base xvfb x11-utils

RUN dpkg --add-architecture i386 && apt-get update && apt-get install wine32 -y

RUN mkdir /saves && mkdir /settings

RUN chown -R steam /saves && chown steam /settings

ENV DISPLAY=:99

RUN winecfg

USER steam

RUN ./steamcmd.sh +@sSteamCmdForcePlatformType windows +login anonymous +app_update 1829350 validate +quit

COPY ./data/templates /templates

COPY entrypoint.sh /

USER root

RUN chmod +x /entrypoint.sh

USER steam

ENTRYPOINT [ "/entrypoint.sh" ]
