# v-rising-wine

## AWS Lightsail
Open UDP Ports (if you don't change the default 9876,9877) for the instance
Create a static IP Adress for your instance
## Installing docker on AWS Lightsail
``sudo yum update``

---

Install docker

``sudo yum install docker``

---

Add ec2-user to docker group

``sudo usermod -a -G docker ec2-user``

---

Install docker-compose

``sudo pip3 install docker-compose``

---

Add docker to start everytime the instance is started

``sudo systemctl enable docker.service``

---

Start the server

``sudo systemctl start docker.service``

---

Check if service is running

``sudo systemctl status docker.service``

---

Reboot

``sudo reboot``

---

After the server is back up again check if service is running

``sudo systemctl status docker.service``


## Starting V Rising server Option one using Env Vars
``mkdir saves``

---

``wget https://gitlab.com/projectmagaming/v-rising-wine/-/raw/main/docker-compose.yaml``

---

``vi docker-compose.yaml``

---

Change the settings to your liking

``docker-compose up``

## Starting V Rising server Option Two using configuration files

``mkdir saves``

``mkdir settings``

---

``wget -O docker-compose.yaml https://gitlab.com/projectmagaming/v-rising-wine/-/raw/main/docker-compose-settings.yaml``

---
``cd settings``

``wget https://gitlab.com/projectmagaming/v-rising-wine/-/raw/main/data/base/ServerGameSettings.json``

``wget https://gitlab.com/projectmagaming/v-rising-wine/-/raw/main/data/base/ServerHostSettings.json``

``wget https://gitlab.com/projectmagaming/v-rising-wine/-/raw/main/data/base/ServerVoipSettings.json``

Change the settings to your liking

---
``cd ..``

``vi docker-compose.yaml``

## adminlist.txt support
Like before using directly the configuration files, you can also create an adminlist.txt file with the steam user ids in 
the **settings** folder 

## Settings
| Parameter      | Default Value | Options |
| ----------- | ----------- | ----------- |
| V_RISING_NAME       |        |Mandatory Name of the server|
| V_RISING_PASSW    |         |Password for the Server|
| V_RISING_SAVE_NAME    |    world1     |Mandatory Name of the save file|
| V_RISING_PUBLIC_LIST    | true        ||
| V_RISING_DESC   |         ||
| V_RISING_SETTING_PRESET   |       ||


| Parameter                     | Default Value | Options  |
|-------------------------------|---------------|----------|
| V_RISING_DAY_DURATION_SECONDS | 1080.0        ||     |
| V_RISING_DAY_START_HOUR       | 9             ||     |
| V_RISING_DAY_END_HOUR         | 17            ||     |
| V_RISING_PORT                 | 9876          ||     |
| V_RISING_QUERY_PORT           | 9877          ||     |
| V_RISING_MAX_USER             | 40            ||     |
| V_RISING_CLAN_SIZE            | 4             ||     |
| V_RISING_GAME_MODE            | PvP           | PvP, PvE |

