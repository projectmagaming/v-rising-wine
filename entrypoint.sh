#!/bin/sh
GAME_DIR=/home/steam/Steam/steamapps/common/VRisingDedicatedServer
SETTINGS_DIR=$GAME_DIR/VRisingServer_Data/StreamingAssets/Settings
check_variable_settings() {
if [ -z "${V_RISING_NAME}" ]; then
    echo "V_RISING_NAME has to be set"

    exit
else
    echo "V_RISING_NAME is ${V_RISING_NAME}"    
fi
}

setServerHostSettings() {
    envsubst < /templates/ServerHostSetting.templ >> $SETTINGS_DIR/ServerHostSettings.json
}

setServerGameSettings() {
    envsubst < /templates/ServerGameSettings.templ >> $SETTINGS_DIR/ServerGameSettings.json
}

setServerVoipSettings() {
    envsubst < /templates/ServerVoipSettings.templ >> $SETTINGS_DIR/ServerVoipSettings.json
}

if [ ! -f "/settings/ServerGameSettings.json" ]; then
  check_variable_settings
fi

./steamcmd.sh +@sSteamCmdForcePlatformType windows +login anonymous +app_update validate 1829350 +quit

rm $SETTINGS_DIR/ServerHostSettings.json
rm $SETTINGS_DIR/ServerGameSettings.json
rm $SETTINGS_DIR/ServerVoipSettings.json

if [ ! -f "/settings/ServerGameSettings.json" ]; then
  echo "ServerGameSettings Configuration not found creating from template"
  setServerGameSettings
else
  echo "ServerGameSettings Configuration found"
  cp /settings/ServerGameSettings.json $SETTINGS_DIR/ServerGameSettings.json
fi

if [ ! -f "/settings/ServerHostSettings.json" ]; then
  echo "ServerHostSettings Configuration not found creating from template"
  setServerHostSettings
else
  echo "ServerHostSettings Configuration found"
  cp /settings/ServerHostSettings.json $SETTINGS_DIR/ServerHostSettings.json
fi

if [ ! -f "/settings/ServerVoipSettings.json" ]; then
  echo "ServerVoipSettings Configuration not found creating from template"
  setServerVoipSettings
else
  echo "ServerVoipSettings Configuration found"
  cp /settings/ServerVoipSettings.json $SETTINGS_DIR/ServerVoipSettings.json
fi

if [ -f "/settings/adminlist.txt" ]; then
  echo "adminlist.txt found"
  cp /settings/adminlist.txt $SETTINGS_DIR/adminlist.txt
fi

cd $GAME_DIR
xvfb-run -a wine ./VRisingServer.exe -persistentDataPath Z:\\saves
